using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWCU_22
{
    class TestStatic
    {
        public static int num;  //Create a static member in a class
        public void count()
        {
            num++;              //increment the value  
        }

        public static int getnum()
        {
            return num;
        }

    }

    class StaticTester
    {
        static void Main(string[] args)
        {
            TestStatic TS = new TestStatic();
            TestStatic TS1 = new TestStatic();

            TS.count();
            TS.count();

            TS1.count();
            TS1.count();

            Console.WriteLine("The value of :{0}", TestStatic.getnum());
            Console.WriteLine("The value of :{0}", TestStatic.getnum());

            Console.ReadKey();


        }

    }
}

